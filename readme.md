




## Deployment

Answer No.1

```bash
  python3 Colorfilter.py
```



Answer No.2

```bash
  python3 Blackwhite.py
```


Output 1
```
Index: 1, Contrast Ratio: 0.019, Lightness: medium, Color Type: colorful
 00000 
Index: 5, Contrast Ratio: 0.015, Lightness: dark, Color Type: colorful
 00000 
Index: 8, Contrast Ratio: 0.020, Lightness: medium, Color Type: colorful
 00000 
Index: 9, Contrast Ratio: 0.013, Lightness: dark, Color Type: colorful
 00000 
Index: 10, Contrast Ratio: 0.011, Lightness: dark, Color Type: colorful
 00000 
Index: 11, Contrast Ratio: 0.012, Lightness: dark, Color Type: colorful
 00000 
Index: 12, Contrast Ratio: 0.010, Lightness: dark, Color Type: colorful
 00000 
Index: 13, Contrast Ratio: 0.013, Lightness: dark, Color Type: colorful
 00000 
Index: 14, Contrast Ratio: 0.011, Lightness: dark, Color Type: colorful
 00000 
Index: 16, Contrast Ratio: 0.021, Lightness: medium, Color Type: colorful
 00000 
Index: 17, Contrast Ratio: 0.010, Lightness: dark, Color Type: colorful
 00000 
Index: 19, Contrast Ratio: 0.022, Lightness: dark, Color Type: colorful
 00000 
Index: 20, Contrast Ratio: 0.025, Lightness: light, Color Type: colorful
 00000 
Index: 21, Contrast Ratio: 0.022, Lightness: dark, Color Type: colorful
 00000 
Index: 22, Contrast Ratio: 0.026, Lightness: light, Color Type: colorful
 00000 
Index: 23, Contrast Ratio: 0.011, Lightness: dark, Color Type: colorful
 00000 
```


Output 2 

```
--------- Black/white color ---------
 Index ke-2 value (67, 76, 73)
 00000 
 Index ke-3 value (99, 105, 93)
 00000 
 Index ke-18 value (140, 132, 139)
 00000 

 ```
 ```
--------- Colorful ---------
 Index ke-1 value (34, 203, 55)
 00000 
 Index ke-5 value (144, 89, 54)
 00000 
 Index ke-6 value (22, 20, 18)
 00000 
 Index ke-7 value (10, 40, 50)
 00000 
 Index ke-8 value (171, 180, 211)
 00000 
 Index ke-9 value (150, 150, 90)
 00000 
 Index ke-10 value (50, 150, 150)
 00000 
 Index ke-11 value (209, 109, 107)
 00000 
 Index ke-12 value (111, 117, 212)
 00000 
 Index ke-13 value (214, 113, 165)
 00000 
 Index ke-14 value (45, 137, 212)
 00000 
 Index ke-15 value (182, 240, 245)
 00000 
 Index ke-16 value (199, 184, 72)
 00000 
 Index ke-17 value (204, 75, 193)
 00000 
 Index ke-19 value (87, 76, 63)
 00000 
 Index ke-20 value (170, 209, 167)
 00000 
 Index ke-21 value (1, 90, 20)
 00000 
 Index ke-22 value (174, 214, 174)
 00000 
 Index ke-23 value (196, 106, 112)
 00000 
 Index ke-25 value (48, 35, 46)
 00000 
```


## Used By

This project is used by the following companies:

- Muhammad Reza Maulana
- TenX

