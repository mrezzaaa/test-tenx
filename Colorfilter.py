data_rgb = [
    (34, 203, 55),
    (67, 76, 73),
    (99, 105, 93),
    (178, 173, 169),
    (144, 89, 54),
    (22, 20, 18),
    (10, 40, 50),
    (171, 180, 211),
    (150, 150, 90),
    (50, 150, 150),
    (209, 109, 107),
    (111, 117, 212),
    (214, 113, 165),
    (45, 137, 212),
    (182, 240, 245),
    (199, 184, 72),
    (204, 75, 193),
    (140, 132, 139),
    (87, 76, 63),
    (170, 209, 167),
    (1, 90, 20),
    (174, 214, 174),
    (196, 106, 112),
    (173, 166, 167),
    (48, 35, 46)
]


base_color = (128, 128, 128) #grey based color
upper_contrast = 0.034
lower_contrast = 0.010
upper_lightness = 0.5
lower_lightness = 0.3
saturation_threshold = 0.15  

def relative_luminance(rgb):
    r, g, b = rgb
    r_linear = r / 255.0 if r <= 10 else ((r / 255.0) ** 2.4)
    g_linear = g / 255.0 if g <= 10 else ((g / 255.0) ** 2.4)
    b_linear = b / 255.0 if b <= 10 else ((b / 255.0) ** 2.4)
    return 0.2126 * r_linear + 0.7152 * g_linear + 0.0722 * b_linear

def contrast_ratio(rgb1, rgb2):
    luminance1 = relative_luminance(rgb1)
    luminance2 = relative_luminance(rgb2)
    
    brighter = max(luminance1, luminance2)
    darker = min(luminance1, luminance2)
    
    return (brighter + 0.05) / (darker + 0.05)

def detect_lightness(rgb):
    luminance = relative_luminance(rgb)
    if luminance > upper_lightness:
        return "light"
    elif luminance < lower_lightness:
        return "dark"
    else:
        return "medium"

def detect_saturation(rgb):
    r, g, b = [x / 255.0 for x in rgb]
    cmax = max(r, g, b)
    cmin = min(r, g, b)
    delta = cmax - cmin
    
    if cmax == 0:
        return 0
    else:
        return delta / cmax

def print_rgb_color(r, g, b):
    # Escape sequence to set foreground color
    color_code = f"\033[38;2;{r};{g};{b}m"
    # Reset escape sequence to clear formatting after printing
    reset_code = "\033[0m"
    print(f"{color_code} 00000 {reset_code}")

# Calculate and print contrast ratio for each color
for index, i in enumerate(data_rgb):
    ratio = contrast_ratio(base_color, i)
    ratio = ratio / 100
    lightness = detect_lightness(i)
    saturation = detect_saturation(i)
    

    if saturation > saturation_threshold:
        color_type = "colorful"
    else:
        color_type = "pastel"

    if( (color_type == "colorful" ) and (ratio < upper_contrast and ratio > lower_contrast) ):
        print(f"Index: {index+1}, Contrast Ratio: {ratio:.3f}, Lightness: {lightness}, Color Type: {color_type}")
        print_rgb_color(i[0], i[1], i[2])



